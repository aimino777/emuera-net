# Emuera.NET 
著作者：VVII, MinorShift, 妊）|дﾟ)の中の人  
頒布者：VVII  
連絡先：eraMegaten Discordサーバー（https://discord.gg/yQRYkNMuWr） VVII 宛  

# 動作環境
.NET 8環境が必要です。

起動時に.NET 8のインストールが確認できなかった場合
自動でインストーラーが立ち上がりますので
画面の指示に従ってインストールしてください。

# 使用方法
『Emuera.exe』『libSkiaSharp.dll』の2つをERBフォルダ・CSVフォルダのある階層に置いたのち、
『Emuera.exe』をダブルクリックして起動してください

デバッグモードでの起動には、同梱の『デバックモード起動.bat』を上記と同じフォルダに置いてダブルクリックしてください。

# 追加機能
実行ファイルと同じ階層に『patch_versions』というフォルダがある場合、
フォルダ内のテキストファイルの中身をログ出力時に書き出します。  
複数ファイルがある場合はファイル名順に書き出します。

# 不具合等連絡先
eraMegaten Discordサーバー（https://discord.gg/yQRYkNMuWr） にて、VVII宛にメッセージを送ってください。

※MinorShift氏、 妊）|дﾟ)の中の人氏は本バージョンの開発には携わっておりません